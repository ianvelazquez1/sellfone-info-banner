import { html, LitElement } from 'lit-element';
import style from './sellfone-info-banner-styles.js';

class SellfoneInfoBanner extends LitElement {
  static get properties() {
    return {
        items:{
          type:Array,
          attribute:"items"
        }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
  }

  render() {
    return html`
       <div class="container">
         ${this.items.map(item=>{
           return html`
           <div class="subContainer">
              <img src="${item.src}" alt="${item.text}" class="infoImage">
              <p class="text">${item.text}</p>
           </div>
           `
         })}
       </div>
      `;
    }
}

window.customElements.define("sellfone-info-banner", SellfoneInfoBanner);
