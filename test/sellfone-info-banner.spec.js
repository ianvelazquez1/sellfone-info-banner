/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-info-banner.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-info-banner></sellfone-info-banner>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
