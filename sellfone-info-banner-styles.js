import { css } from 'lit-element';

export default css`
:host {
  display: inline-block;
  box-sizing: border-box;
  width: 100%;
}

:host([hidden]), [hidden] {
  display: none !important;
}

*, *:before, *:after {
  box-sizing: inherit;
  font-family: inherit;
}

.container{
  width: 100%;
  border-top: 1px solid #CCD2DA;
  border-bottom: 1px solid #CCD2DA;
  display: grid;
  grid-template-columns: 25% 25% 25% 25%;
  align-items: center;
}

.subContainer{
  display: flex;
  flex-direction:  column;
  flex-wrap: nowrap;
  font-size: 135%;
  
  align-items: center;
  
}

.infoImage{
  max-width: 15%;
  margin-left:10px;
  margin-top: 20px;
}

.text{
  margin-left:10px;
}
`;
